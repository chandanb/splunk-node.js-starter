// cors configurations
module.exports.corsOptions = {
    orgin: "*",
    optionsSuccessStatus: 200
}

// splunk local dev config
module.exports.splunkLocalConfig = {
    username:"admin",
    password:"carbynetech123",
    scheme:"https",
    host:"13.126.171.55",
    port:"8089",
    version:"6.6.1"
}

// splunk server prod config
module.exports.splunkServerConfig = {
    username:"admin",
    password:"carbynetech123",
    scheme:"https",
    host:"localhost",
    port:"8089",
    version:"6.6.1"
}

module.exports.port = 9006;