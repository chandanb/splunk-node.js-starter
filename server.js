const express = require('express');
const cors = require('cors');
const appConfig = require('./config/app-config');
const  app = express();
app.use(cors(appConfig.corsOptions));
const  server = require('http').createServer(app);
const splunkjs = require('splunk-sdk');
const port = process.env.port || appConfig.port;
const splunkService = require('./services/splunk-service');

// routes for API
const router = express.Router();  // get an instance of express router

// test API
router.get('/test', (request, response) => {
  response.status(200).send("Test Success");
})

router.get('/testQuery', (request, response) => {
  splunkService.testQuery().then(
    data => {
      // console.log("Response Test", data)
      response.status(200).send(data);
    }
  )
  .catch(error => {
    console.log(error);
    response.status(400).send(error);
  })
})

// REGISTER ALL THE ROUTERS HERE 
// All the API's will be prefixed with /api
app.use('/api', router);

/**
 * @desc test func to check the splunk connectivity with the node app
 */
function listApps() {
  console.log("Fetching apps...");
  splunkService.listapps().then(
    data => {
      console.log("App List fetched", data);
    }
  )
  .catch(error => {
    console.log(error);
  })
}

// listApps();

// listen to incomming requests
server.listen(port, function () {
    console.log('Server started on *: ' + port);
  });