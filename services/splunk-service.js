const splunkjs = require('splunk-sdk');
const appConfig = require('../config/app-config');
const util = require('util');
const testModel = require('../model/test-model');
// instantiate a splunk service
const splunkService = new splunkjs.Service(appConfig.splunkLocalConfig);

module.exports.listapps = async () => {
    const appList = await getApps();
    return appList;
}

// Define all query handler functions here

module.exports.testQuery = async () => {
    const query = testModel.testQuery()
    const resData = await executeQuery(query);
    return resData;
}

/**
 * @desc test method to check splunk connectivity
 */
function getApps() {
    return new Promise((resolve, reject) => {
        splunkService.apps().fetch(function (err, apps) {
            if (err) {
                console.log("Error retrieving apps: ", err);
                reject("Failed to fetch App list")
            }    
            var appsList = apps.list();
            resolve(appsList);
        }); 
    })
}



/**
 * @desc executes splunk query and returns data
 * @param {string} searchQuery 
 */
function executeQuery(searchQuery) {
    return new Promise((resolve, reject) => {
        var searchParams = {
            exec_mode: "blocking",
            // earliest_time: "2017-02-01T16:27:43.000-07:00",
            // resultCount: 50000,
            // status_buckets: 300,
            // max_count: 50000,
            // eventCount: 50000
        };
        splunkService.login((error, response) => {
            if (error) {
                reject({ code: 401, message: "Unauthorized" });
            }
        })
        splunkService.search(
            searchQuery,
            searchParams,
            (error, job) => {
                if (error) {
                    reject({ code: 400, message: "Failed to fetch data" });
                }
                // fetch the job for data processing
                job.fetch(error => {
                    if (error) {
                        reject({ code: 400, message: "Failed to fetch data" })
                    }
                })
                // get the results from the jo
                job.results({}, (error, results) => {
                    const fields = results.fields;
                    // console.log("Fields", fields);
                    const rows = results.rows;
                    // console.log("Rows", rows);
                    var responseData = [];
                    responseData.push({
                        rows: rows,
                        columns: fields
                    })
                    resolve(responseData);
                })
            }
        )
    })

}

module.exports.myData = "MY Data";